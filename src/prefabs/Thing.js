import Phaser from 'phaser';

export default function Thing(state, data) {
    const thing = new Phaser.Sprite(state.game, data.x, data.y, data.asset);
    thing.game = state.game;
    thing.data = data;
    thing.state = state;
    thing.anchor.setTo(0.5);

    thing.inputEnabled = true;
    thing.input.pixelPerfectClick = true;

    thing.events.onInputDown.add(Thing.prototype.touch, thing);

    return thing;
}

Thing.prototype.touch = function () {
    const { selectedItem } = this.state;
    this.state.panelLabel.text = this.data.text;
    if (this.data.type === 'collectable') {
        this.state.addItem(this.data);
        this.kill();
        return true;
    } else if (this.data.type === 'door' && this.data.isOpen) {
        const playerData = {
            room: this.data.destination,
            items: []
        };
        this.state.items.forEachAlive(item => playerData.items.push(item.data));
        this.game.state.start('Game', true, false, playerData);
        return true;
    }
    if (selectedItem && this.data.interactions && this.data.interactions[selectedItem.data.id]) {
        const interaction = this.data.interactions[selectedItem.data.id];
        if (interaction.text) {
            this.state.panelLabel.text = interaction.text;
        }
        if (interaction.asset) {
            this.loadTexture(interaction.asset);
            this.data.asset = interaction.asset;
        }
        if (interaction.action === 'open-door') {
            this.data.isOpen = true;
            selectedItem.kill();
            this.state.clearSelection();
        }
    }

    return true;
};

