import Phaser from 'phaser';

export default (state, x, y, data) => {
    const item = new Phaser.Sprite(state.game, x, y, data.asset);
    item.game = state.game;
    item.state = state;
    item.data = data;
    item.anchor.setTo(0.5);

    item.inputEnabled = true;
    item.input.pixelPerfectClick = true;
    item.events.onInputDown.add(item.state.selectItem, item.state);

    return item;
};
