/* eslint-disable no-unused-vars */
/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
import PIXI from 'pixi';
import P2 from 'p2';
import Phaser from 'phaser';
import Boot from './states/BootState';
import Preload from './states/PreloadState';
import Game from './states/GameState';
import '../assets/css/style.css';

const PointAndClick = {};
PointAndClick.game = new Phaser.Game(640, 360, Phaser.AUTO);
PointAndClick.game.state.add('Boot', Boot());
PointAndClick.game.state.add('Preload', Preload());
PointAndClick.game.state.add('Game', Game());
PointAndClick.game.state.start('Boot');
