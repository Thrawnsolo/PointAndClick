import Phaser from 'phaser';

import bedroom from '../../assets/images/bedroom/bedroom.png';
import chair from '../../assets/images/bedroom/chair.png';
import medal from '../../assets/images/bedroom/flat_medal6.png';
import gem from '../../assets/images/bedroom/gem.png';
import woodenChair from '../../assets/images/bedroom/wooden-chair-viyana.png';

import bluePanel from '../../assets/images/blue_panel.png';

import armlessChair from '../../assets/images/livingroom/armless-chair.png';
import bomb from '../../assets/images/livingroom/bomb.png';
import door from '../../assets/images/livingroom/door.png';
import fancyTable from '../../assets/images/livingroom/fancy-table.png';
import key from '../../assets/images/livingroom/key.png';
import lamp from '../../assets/images/livingroom/lamp.png';
import lampOff from '../../assets/images/livingroom/lamp-turn-off.png';
import livingroom from '../../assets/images/livingroom/livingroom.png';
import openDoor from '../../assets/images/livingroom/opendoor.png';
import tv from '../../assets/images/livingroom/tv.png';
import weight from '../../assets/images/livingroom/weight.png';


import bedroomData from '../../assets/data/bedroom.json';

const imagesAssets = [
    bedroom,
    chair,
    medal,
    gem,
    woodenChair,
    bluePanel,
    armlessChair,
    bomb,
    door,
    fancyTable,
    key,
    lamp,
    lampOff,
    livingroom,
    openDoor,
    tv,
    weight
];
const imagesNames = ['bedroom', 'chair', 'medal', 'gem', 'woodenChair', 'bluePanel', 'armlessChair', 'bomb',
    'door', 'fancyTable', 'key', 'lamp', 'lampOff', 'livingroom', 'openDoor', 'tv', 'weight'];

export default () => {
    const obj = new Phaser.State();
    obj.preload = function () {
        this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bar');
        this.preloadBar.anchor.setTo(0.5);
        this.preloadBar.scale.setTo(100, 1);
        this.load.setPreloadSprite(this.preloadBar);

        this.load.images(imagesNames, imagesAssets);
    };
    obj.create = function () {
        this.state.start('Game');
    };
    return obj;
};
