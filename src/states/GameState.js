import Phaser from 'phaser';
import Thing from '../prefabs/Thing';
import Item from '../prefabs/Item';
import livingroomData from '../../assets/data/livingroom.json';
import bedroomData from '../../assets/data/bedroom.json';

const roomData = {
    bedroom: bedroomData,
    livingroom: livingroomData
};

export default () => {
    const gameState = new Phaser.State();

    gameState.init = function (playerData) {
        this.playerData = playerData || {};
        this.playerData.room = roomData[this.playerData.room] || livingroomData;
        this.playerData.items = playerData ? playerData.items : [];
    };
    gameState.loadRoom = function () {
        this.roomData = this.playerData.room;
        this.background = this.add.sprite(0, 0, this.roomData.background);
        this.things = this.add.group();
        this.roomData.things.forEach((thingData) => {
            const thing = Thing(this, thingData);
            this.things.add(thing);
        });
    };
    gameState.create = function () {
        this.panel = this.add.sprite(0, 270, 'bluePanel');
        const style = {
            font: '16px Prstart',
            fill: '#fff',
            align: 'left',
            wordWrap: true,
            wordWrapWidth: 400
        };
        this.panelLabel = this.add.text(10, 290, '', style);
        this.loadRoom();
        this.items = this.add.group();
        this.showItems();
    };

    gameState.addItem = function (itemData) {
        const item = Item(this, 420 + (this.items.length * 80), 310, itemData);
        this.items.add(item);
        return item;
    };

    gameState.selectItem = function (item) {
        if (this.selectedItem !== item) {
            this.clearSelection();
            this.selectedItem = item;
            this.selectedItem.alpha = 0.5;
        } else {
            this.clearSelection();
        }
    };

    gameState.clearSelection = function () {
        this.selectedItem = null;
        this.items.setAll('alpha', 1);
    };

    gameState.showItems = function () {
        this.playerData.items.forEach( itemData => this.addItem(itemData));
    };

    return gameState;
};
