import Phaser from 'phaser';
import bar from '../../assets/images/preloader-bar.png';

export default () => {
    const obj = new Phaser.State();
    obj.init = function () {
        this.game.stage.backgroundColor = '#fff';
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
    };
    obj.preload = function () {
        this.load.image('bar', bar);
    };
    obj.create = function () {
        this.state.start('Preload');
    };
    return obj;
};
